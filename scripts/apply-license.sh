#!/bin/bash

applyLicense() {
  . ./get-token.sh

  local GITLAB_LICENSEE
  local LOOP_COUNT=0
  echo -n 'Adding Enterprise Gitlab License'
  while [ -z "$GITLAB_LICENSEE" ] && (( LOOP_COUNT < 10 )); do
    echo -n .
    let LOOP_COUNT=LOOP_COUNT+1
    sleep 2
    echo "ACCESS_TOKEN: $ACCESS_TOKEN"
    curl --verbose --insecure -X POST --header "Authorization: Bearer $ACCESS_TOKEN" "$EXTERNAL_URL/api/v4/license?license=$GITLAB_LICENSE"
    GITLAB_LICENSEE=$(curl --insecure -X POST --header "Authorization: Bearer $ACCESS_TOKEN" "$EXTERNAL_URL/api/v4/license?license=$GITLAB_LICENSE" 2> /dev/null |jq --raw-output .licensee)
  done
  if (( LOOP_COUNT < 10 )); then
    echo 'Successfully Added'
  else
    echo 'Failed'
    exit 1
  fi
}

applyLicense
