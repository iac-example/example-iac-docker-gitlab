#!/bin/sh

wait() {
  local READY
  echo -n 'Waiting for Gitlab to be Ready'
  while [ -z "$READY" ]; do
    echo -n .
    sleep 5
    READY=$(docker service ls|grep DEVOPS_omnibus|grep 1/1)
  done
  echo Done
}

wait
