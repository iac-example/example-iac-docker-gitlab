#!/bin/bash

addPermissions() {
  local gitlabGroupID=$1
  local activeDirectoryGroup=$2
  local gitlabRoleLevel=$3

  echo -n "Adding the \"$activeDirectoryGroup\" AD group to Gitlab group ID $gitlabGroupID as Gitlab role level $gitlabRoleLevel"
  local LDAP_GROUP_LINKS=$(curl --insecure --header "Authorization: Bearer $ACCESS_TOKEN" -X GET "$EXTERNAL_URL/api/v4/groups/$gitlabGroupID" 2> /dev/null|jq --raw-output ".ldap_group_links")
  local GROUP_NAME_MATCH
  if [ ! -z "$LDAP_GROUP_LINKS" ] && [ "$LDAP_GROUP_LINKS" != 'null' ]; then
      GROUP_NAME_MATCH=$(echo $LDAP_GROUP_LINKS| jq ".[]|select(.cn == \"$activeDirectoryGroup\")|.cn")
  fi

  if [ -z "$GROUP_NAME_MATCH" ]; then
    local LOOP_COUNT=0
    while [ "$ROLE_ASSIGNED" != "$gitlabRoleLevel" ] && (( LOOP_COUNT < 10 )); do
      echo -n .
      let LOOP_COUNT=LOOP_COUNT+1
      sleep 2
      local ROLE_ASSIGNED=$(curl --insecure --header "Authorization: Bearer $ACCESS_TOKEN" -d "cn=$activeDirectoryGroup&group_access=$gitlabRoleLevel&provider=ldapmain" -X POST "$EXTERNAL_URL/api/v4/groups/$gitlabGroupID/ldap_group_links" 2> /dev/null|jq .group_access)
    done
    if (( LOOP_COUNT < 10 )); then
      echo 'Successfully Added'
    else
      echo 'Failed'
      exit 1
    fi
  else
      echo '...Already Exists'
  fi
}

addGroup() {
  . ./scripts/get-token.sh
  echo "ACCESS_TOKEN: $ACCESS_TOKEN"

  local GROUP_NAME=$(cat $PROJECT_DEFINITION_FILE|jq --raw-output .name)
  echo '###############################################################################'
  echo "# Processing $GROUP_NAME"
  echo '###############################################################################'
  echo -n 'Checking to see if group already exists...'
  local SEARCH_RESULTS=$(curl --insecure --verbose -d "search=$GROUP_NAME" -X GET --header "Authorization: Bearer $ACCESS_TOKEN" "$EXTERNAL_URL/api/v4/groups" 2> /dev/null|jq --raw-output .[0])
  local GROUP_NAME_RETURNED=$(echo $SEARCH_RESULTS|jq --raw-output .name)
  local GROUP_ID
  if [ "$GROUP_NAME" != "$GROUP_NAME_RETURNED" ]; then
    echo No
    local LOOP_COUNT=0
    echo -n "Adding $GROUP_NAME group"
    while [ -z "$GROUP_ID" ] && (( LOOP_COUNT < 10 )); do
      echo -n .
      let LOOP_COUNT=LOOP_COUNT+1
      sleep 2
      GROUP_ID=$(curl --insecure -X POST -H "Authorization: Bearer $ACCESS_TOKEN" -H "Content-Type: application/json" -d @$PROJECT_DEFINITION_FILE "$EXTERNAL_URL/api/v4/groups" 2> /dev/null|jq .id)
    done
    if (( LOOP_COUNT < 10 )); then
      echo 'Successfully Added'
    else
      echo 'Failed'
      exit 1
    fi
  else
    echo '...Yes'
    GROUP_ID=$(echo $SEARCH_RESULTS|jq .id)
  fi

  local ACCESS_LVL_LEN=$(cat $PERMISSIONS_DEFINITION_FILE| jq ".|length")
  local CURRENT_LVL=0
  while (( CURRENT_LVL < ACCESS_LVL_LEN )); do
    local ACCESS_LVL=$(cat $PERMISSIONS_DEFINITION_FILE| jq -r ".[$CURRENT_LVL].gitlabRoleLevel")
    local AD_GROUP_LEN=$(cat $PERMISSIONS_DEFINITION_FILE| jq ".[]|select(.gitlabRoleLevel == $ACCESS_LVL)|.activeDirectoryGroups|length")
    echo
    echo "Adding $AD_GROUP_LEN AD groups to Gitlab role level $ACCESS_LVL"
    local CURRENT_GROUP=0
    while (( CURRENT_GROUP < AD_GROUP_LEN )); do
      local AD_GROUP=$(cat $PERMISSIONS_DEFINITION_FILE| jq -r ".[]|select(.gitlabRoleLevel == $ACCESS_LVL)|.activeDirectoryGroups|.[$CURRENT_GROUP]")
      addPermissions $GROUP_ID "$AD_GROUP" $ACCESS_LVL
      let CURRENT_GROUP=CURRENT_GROUP+1
    done
    let CURRENT_LVL=CURRENT_LVL+1
  done
  echo
}

addGroup
