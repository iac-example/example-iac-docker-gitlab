#!/bin/bash

getToken() {
  local LOOP_COUNT=0
  echo -n 'Retrieving Access Token'
  while [ -z "$ACCESS_TOKEN" ] && (( LOOP_COUNT < 10 )); do
    echo -n .
    let LOOP_COUNT=LOOP_COUNT+1
    sleep 2
    ACCESS_TOKEN=$(curl --insecure -d "grant_type=password&username=root&password=$GITLAB_ROOT_PASSWORD" -X POST "$EXTERNAL_URL/oauth/token" 2> /dev/null |jq --raw-output .access_token)
  done
  if (( LOOP_COUNT = 0 )); then
    echo '...Reusing Existing Token'
  elif (( LOOP_COUNT < 10 )); then
    echo 'Successfully Retrieved'
  else
    echo 'Failed'
    exit 1
  fi
}

getToken
