#!/bin/bash

disableSignup() {
  . ./scripts/get-token.sh

  local SIGNUP_ENABLED=true
  local LOOP_COUNT=0
  echo -n 'Disabling Self-Service Sign-up'
  while [ "$SIGNUP_ENABLED" != 'false' ] && (( LOOP_COUNT < 10 )); do
    echo -n .
    let LOOP_COUNT=LOOP_COUNT+1
    sleep 2
    echo "ACCESS_TOKEN: $ACCESS_TOKEN"
    curl --verbose --insecure -H "Authorization: Bearer $ACCESS_TOKEN" -d "signup_enabled=false" -X PUT "$EXTERNAL_URL/api/v4/application/settings"
    SIGNUP_ENABLED=$(curl --insecure -H "Authorization: Bearer $ACCESS_TOKEN" -d "signup_enabled=false" -X PUT "$EXTERNAL_URL/api/v4/application/settings" 2> /dev/null|jq .signup_enabled)
    echo "SIGNUP_ENABLED: $SIGNUP_ENABLED"
  done
  if (( LOOP_COUNT < 10 )); then
    echo 'Successfully Disabled'
  else
    echo 'Failed'
    exit 1
  fi
}

disableSignup
